#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "pair.h"
#include "matrix.h"


int main(){
    struct matrix *A;
    struct matrix *B;
    struct matrix *C;

    A = matrixInput("data/A.txt");
    viewMatrix(A,"A :");

    B = matrixInput("data/B.txt");
    viewMatrix(B,"B :");

    printf("\nAddition des matrices :\n");
    if( (A->n == B->n) && (A->m == B->m) ){
        C = addMatrix(A,B);
        viewMatrix(C,"A + B :");
        saveMatrix(C,"data/ResAdd.txt");
    }else{
        printf("Les matrices n'ont pas des dimensions compatibles\n");
    }


    printf("\nMultiplication des matrices :\n");
    if( A->m == B->n ){
        C = multMatrix(A,B);
        viewMatrix(C,"A * B :");
        saveMatrix(C,"data/ResMult.txt");
    }else{
        printf("Les matrices n'ont pas des dimensions compatibles\n");
    }


    return 0;
}
