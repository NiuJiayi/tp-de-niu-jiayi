#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "elmlist.h"
#include "list.h"

/*********************************
DÉCLARATIONS DES FONCTIONS PRIVÉES
*********************************/
void insert_after(struct list * L, void * data, struct elmlist * ptrelm);

/**********************************
DÉFINITIONS DES FONCTIONS PUBLIQUES
**********************************/
struct list * new_list() {
  struct list * L = (struct list *) calloc(1, sizeof(struct list));
  assert(L != NULL);
  return L;
}

void del_list(struct list * L, void (*ptrf) ()) {
  assert(L);
  for(struct elmlist * iterator = L->head; iterator; ) {
    struct elmlist * E = iterator;
    iterator = iterator->suc;
    del_elmlist(&E, ptrf);
  }
  free(L);
}

bool is_empty(struct list * L) {
  assert(L != NULL);
  return L->numelm == 0;
}

struct elmlist * get_head(struct list * L) {
  assert(L != NULL);
  return L->head;
}

struct elmlist * get_tail(struct list * L) {
  assert(L != NULL);
  return L->tail;
}

void cons(struct list * L, void * data) {
  assert(L != NULL);
  struct elmlist * E = new_elmlist(data);
  E->suc = L->head;
  if( L->head ) {
    L->head->pred = E;
  } else {
    L->tail = E;
  }
  L->head = E;
  L->numelm += 1;
}

void queue(struct list * L, void * data) {
  assert(L != NULL);
  struct elmlist * E = new_elmlist(data);
  E->pred = L->tail;
  if ( L->tail ) {
    L->tail->suc = E;
  } else {
    L->head = E;
  }
  L->tail = E;
  L->numelm += 1;
}

void insert_ordered(struct list * L, void * data, bool (*cmp_ptrf) ()) {
  if( is_empty(L) || (*cmp_ptrf)(data, get_data(get_head(L))) ) {
    cons(L, data);
  } else if( (*cmp_ptrf)(get_data(get_tail(L)), data) ) {
    queue(L, data);
  } else {
    struct elmlist * iterator = L->head;
    bool found = false;
    while(iterator && !found) {
      found = (*cmp_ptrf)(data, get_data(iterator));
      if(!found) {
        iterator = iterator->suc;
      }
    }
    insert_after(L, data, iterator->pred);
  }
}

void view_list(struct list * L, void (*ptrf)()) {
  printf("\t\t====================\n");
  printf("\t\t|| View data list ||\n");
  printf("\t\t====================\n");
  if(is_empty(L)) {
    printf("[ ] //empty list\n");
  }
  else {
    struct elmlist * iterator = L->head;
    while(iterator) {
      view_elmlist(iterator, ptrf);
      iterator = iterator->suc;
    }
  }
  printf("\t\t====================\n\n");
}

/*******************************************
Définitions des fonctions privées du TA list
*******************************************/

void insert_after(struct list * L, void * data, struct elmlist * place) {
  assert(L != NULL);
  if( is_empty(L) || !place) {
    cons(L,data);
  } else if( place == get_tail(L)) {
    queue(L, data);
  } else {
    struct elmlist * E = new_elmlist(data);
    E->pred = place;
    if(place == L->tail) {
      L->tail = E;
    } else {
      place->suc->pred = E;
    }
    E->suc = place->suc;
    place->suc = E;
    L->numelm += 1;
  }
}
