#include <stdlib.h>
#include <stdio.h>

#include "list.h"
#include "db.h"
#include "form.h"
#include "outils.h"

int main() {
  void * (* read_ptrf)(FILE *, enum mode_t) = (void * (*)()) &read_form;
  void (* view_ptrf)(struct form_t *) = &view_form;
  void (* write_ptrf)(struct form_t *, enum mode_t, FILE *)  = &write_form;
  void (* del_ptrf)(struct form_t **) = &del_form;
  // Pour l'insertion ordonnée je vous laisse écrire le code...
  // bool (* cmp_ptrf)(struct form *, struct form *) = &lt_form;

  struct list * L = read_list(TEXT, read_ptrf, NULL);
  view_list(L, view_ptrf);
  write_list(L, BINARY, write_ptrf);
  del_list(L, del_ptrf);

  printf("\n\nLa liste a été chargée depuis un fichier texte\nPuis écrite dans un fichier binaire avant d'être détruite\nMaintenant la liste va être chargée depuis le fichier binaire, affichée puis détruite\n\n");
  L = read_list(BINARY, read_ptrf, NULL);
  view_list(L, view_ptrf);
  del_list(L, del_ptrf);

  return EXIT_SUCCESS;
}
