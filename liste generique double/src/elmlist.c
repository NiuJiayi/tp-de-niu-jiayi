#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "elmlist.h"

struct elmlist * new_elmlist(void * data ) {
  struct elmlist * E = (struct elmlist *)calloc(1, sizeof(struct elmlist));
  assert(E != NULL);
  E->data = data;
  return E;
}

void del_elmlist(struct elmlist ** ptrE, void (*ptrf) ()) {
  assert(ptrE && *ptrE);
  void * data = (*ptrE)->data;
  (*ptrf)(&data);
  free(*ptrE);
  *ptrE = NULL;
}

struct elmlist * get_suc(struct elmlist * E) {
  assert(E != NULL);
  return E->suc;
}

struct elmlist * get_pred(struct elmlist * E) {
  assert(E != NULL);
  return E->pred;
}

void * get_data(struct elmlist * E) {
  assert(E != NULL);
  return E->data;
}

void set_suc(struct elmlist * E, struct elmlist * S) {
  assert(E != NULL);
  E->suc = S;
}

void set_pred(struct elmlist * E, struct elmlist * P) {
  assert(E != NULL);
  E->pred = P;
}

void set_data(struct elmlist * E, void * data) {
  assert(E != NULL);
  E->data = data;
}

void view_elmlist(struct elmlist * E, void (*ptrf)()) {
  (*ptrf)(E->data);
}
