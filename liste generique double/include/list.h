#ifndef _LIST_
#define _LIST_

#include <stdbool.h>

/**
Abstract type for double-linked list modeled by
- 2 pointers pointing to the head and the tail of the list
- the number of element the list contains
*/
struct list {
  struct elmlist * head, * tail;
  int numelm;
};

/****************
Constructors & co
****************/
struct list * new_list();

void del_list(struct list * L, void (*ptrf) ());

bool is_empty(struct list * L);

/********************
Accessors & modifiers
********************/
struct elmlist * get_head(struct list * L);

struct elmlist * get_tail(struct list * L);

/** Add on head, add on tail, insert data at place localized by cmp_ptr */
void cons(struct list * L, void * data);

void queue(struct list * L, void * data);

void insert_ordered(struct list * L, void * data, bool (*cmp_ptrf)());

/** Display list on stdout stream */
void view_list(struct list * L, void (*ptrf)());

#endif // _LIST_
